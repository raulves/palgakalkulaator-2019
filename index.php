<?php

require_once("vendor/tpl.php");
require_once("functions.php");

// siia tuleb front-controller

$cmd = "view";

if (isset($_GET["cmd"])) {
    $cmd = $_GET["cmd"];
}

if ($cmd == "calculate") {
    // arvutame ning kuvame tulemuse vaata
    $brutoPalk = $_POST["bruto"];

    $tootajaMaksud = calculateNetSalary($brutoPalk);
    $tooandjaKulud = calculateEmployerExpenses($brutoPalk);

    $data = [
        "subTemplate" => "result.html",
        "tootajaMaksud" => $tootajaMaksud,
        "tooandjaKulud" => $tooandjaKulud,
        "brutopalk" => $brutoPalk
    ];

    print renderTemplate("templates/main.html", $data);

} else {
    // kuvame sisestamine vormi
    $data = ["subTemplate" => "form.html"];
    print renderTemplate("templates/main.html", $data);
}
