<?php

// siia tuleb funktsioon(id) palga ja maksude arvutamiseks

const TooTAJA_TooTUSKINDLUSTUS_MÄÄR = 0.016;
const KOGUMISPENSIONI_MÄÄR = 0.02;
const TULUMAKSU_MÄÄR = 0.2;
const TULUMAKSUVABA_MIINIMUM = 500;
const SOTSIAALMAKSU_MÄÄR = 0.33;
const TooANDJA_TooTUSKINDLUSTUS_MÄÄR = 0.008;

function calculateNetSalary($brutoPalk) {
    $tootjaTootuskindlustus = $brutoPalk * TooTAJA_TooTUSKINDLUSTUS_MÄÄR;
    $kogumispension = $brutoPalk * KOGUMISPENSIONI_MÄÄR;
    $maksustatavTulu = $brutoPalk - TULUMAKSUVABA_MIINIMUM - $tootjaTootuskindlustus - $kogumispension;

    $tulumaks = 0;
    if ($maksustatavTulu > 0) {
        $tulumaks = $maksustatavTulu * TULUMAKSU_MÄÄR;
    }

    $netopalk = $brutoPalk - $tootjaTootuskindlustus - $kogumispension - $tulumaks;

    return ["tootjaTootuskindlustus" => $tootjaTootuskindlustus,
        "kogumispension" => $kogumispension,
        "tulumaks" => $tulumaks,
        "netopalk" => $netopalk];

}

function calculateEmployerExpenses($brutoPalk) {
    $sotsiaalmaks = $brutoPalk * SOTSIAALMAKSU_MÄÄR;
    $tooandjaTootuskindlustus = $brutoPalk * TooANDJA_TooTUSKINDLUSTUS_MÄÄR;
    $kogukulu = $brutoPalk + $sotsiaalmaks + $tooandjaTootuskindlustus;

    return ["sotsiaalmaks" => $sotsiaalmaks,
        "tooandjaTootuskindlustus" => $tooandjaTootuskindlustus,
        "kogukulu" => $kogukulu];

}



